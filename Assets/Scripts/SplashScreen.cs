﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SplashScreen : MonoBehaviour
{
    private float _loadingTime = 0f;
    private float _percentage;
    public TextMeshProUGUI percentageText;
    public Slider loadingSlider;

    void Update()
    {
        //Simulamos una carga de 5 segundos y cuando acaba cargamos la siguiente escena
        if(_loadingTime < 5f)
        {
            _loadingTime += Time.deltaTime;
            _percentage = (_loadingTime/5) * 100;
            percentageText.text = _percentage.ToString("F0") + " %";
            loadingSlider.value = _loadingTime;
        }
        else
        {
            SceneManager.LoadScene("MainScene");
        }
    }
}
