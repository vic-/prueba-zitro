﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using System;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    //URL del reloj donde sacaremos la hora
    private string _timeURL = "http://worldtimeapi.org/api/ip";
    TimeAPI myTime = new TimeAPI();
    public TextMeshProUGUI timeTMP;

    //Prefabs de las particulas y del sprite con animacion
    public GameObject playerPrefab;
    public GameObject particlesPrefab;

    //Variable para saber si estamos en el juego 1, 2 o 3
    private int gameSelected;
    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        StartCoroutine(RequestTime(_timeURL));
    }

    private IEnumerator RequestTime(string uri)
    {
        //Nos descargamos la informacion de la url
        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                Debug.Log(request.error);
                timeTMP.text = request.error;
            }
            else if(request.isDone)
            {
                //Cuando la descarga esta acabada asignamos todos los datos
                myTime = JsonUtility.FromJson<TimeAPI>(request.downloadHandler.text);
                var persedTime = DateTime.Parse(myTime.datetime);
                timeTMP.text = persedTime.ToString("dd-MM-yyyy HH:mm");
            }
        }
    }

    //Funcion para seleccionar que hacen los botones la variable lvl se asigna en el inspector
    public void SelectGame(int lvl)
    {
        //Al pulsar el boton se emite un sonido
        _audioSource.Play();

        switch (lvl)
        {
            //si el valor de lvl es 1 cambia a la escena del juego principal
            case 1:
                SceneManager.LoadScene("GameScene");
            break;
            //si el valor de lvl es 2 Instancia el prefab del sistema de particulas
            case 2:
                Instantiate (particlesPrefab);
            break;
            //si el valor de lvl es 3 instancia el sprite con la animacion
            case 3:
                var newPlayer = Instantiate (playerPrefab);
                newPlayer.transform.SetParent(GameObject.Find("Canvas").transform);
                newPlayer.transform.localPosition = new Vector3(0, 0, 0);
            break;
        }
        //Asignarle el valor a esta variable nos servira para que el boton back sepa lo que tiene que hacer
        gameSelected = lvl;
    }

    public void BackButton()
    {
        _audioSource.Play();

        switch (gameSelected)
        {
            //Si gameSelected es 2 destruye el prefab del sistema de particulas
            case 2:
                Destroy(GameObject.Find("Cloud(Clone)").gameObject);
            break;
            //Si gameSelected es 3 destruye el prefab del sprite
            case 3:
                Destroy(GameObject.Find("Animation(Clone)").gameObject);
            break;
        }
        gameSelected = 0;
    }
}
