﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    //URL del reloj donde sacaremos la hora
    private string _timeURL = "http://worldtimeapi.org/api/ip";
    TimeAPI myTime = new TimeAPI(); 
    public TextMeshProUGUI timeTMP;

    private AudioSource _audioSource;
    public AudioClip[] audioClips;
    
    //Variable para poder cambiar que se pueda interactuar con el boton
    public Button playButton;

    //Gameobjects de los rodillos girando
    public GameObject blur1;
    public GameObject blur2;
    public GameObject blur3;

    //Variables para almacenar las diferentes figuras de cada rodillo
    public Image[] rodillo1;
    public Image[] rodillo2;
    public Image[] rodillo3;

    //Variable para almacenar los sprites de las figuras
    public Sprite[] figuras;

    //Aqui se almacenaran el valor de cada figura de cada rodillo para poder comprobar si tenemos premio
    public int[] figurasRodillo1;
    public int[] figurasRodillo2;
    public int[] figurasRodillo3;

    //Gameobject para activar y desactivar el mensaje de premio
    public GameObject mensajePremio;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        StartCoroutine(RequestTime(_timeURL));
    }

    private IEnumerator RequestTime(string uri)
    {
        //Nos descargamos la informacion de la url
        using (UnityWebRequest request = UnityWebRequest.Get(uri))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                Debug.Log(request.error);
                timeTMP.text = request.error;
            }
            else
            {
                //Cuando la descarga esta acabada asignamos todos los datos
                myTime = JsonUtility.FromJson<TimeAPI>(request.downloadHandler.text);
                var persedTime = DateTime.Parse(myTime.datetime);
                timeTMP.text = persedTime.ToString("dd-MM-yyyy HH:mm");
            }
        }
    }

    public void MainMenu()
    {
        _audioSource.PlayOneShot(audioClips[2]);
        SceneManager.LoadScene("MainScene");
    }

    public void Play()
    {
        //Ponemos que no se pueda interactuar con el boton mientras se esta haciendo la tirada para evitar que se hagan mas tiradas mientras se esta realizando una
        playButton.interactable = false;
        _audioSource.PlayOneShot(audioClips[0]);
        //Si el mensaje de premio esta activado porque hemos ganado en la anterior tirada lo desactivamos
        if(mensajePremio.activeInHierarchy)
        {
            mensajePremio.SetActive(false);
        }
        StartCoroutine(MovimientoRodillos());
    }

    IEnumerator MovimientoRodillos()
    {
        int randomNumber;

        //activamos la animacion para cada rodillo en intervalos de 0.5 segundos
        blur1.SetActive(true);
        //Hacemos un loop para asignar los sprites y el valor de cada figura en el rodillo
        for (int i = 0; i < rodillo1.Length; i++)
        {
            //Asignamos un valor aleatorio a la variable que servira para asignar un sprite a cada figura del rodillo
            //Como hay 3 figuras distintas ponemos un valor aleatorio entre 0 y 3
            randomNumber = UnityEngine.Random.Range(0,3);
            //Asignamos el sprite
            rodillo1[i].sprite = figuras[randomNumber];
            //Aqui almacenamos el valor para mas adelante saber si tenemos premio
            figurasRodillo1[i] = randomNumber;
        }
        yield return new WaitForSecondsRealtime(0.5f);
        blur2.SetActive(true);
        for (int i = 0; i < rodillo1.Length; i++)
        {
            randomNumber = UnityEngine.Random.Range(0,3);
            rodillo2[i].sprite = figuras[randomNumber];
            figurasRodillo2[i] = randomNumber;
        }
        yield return new WaitForSecondsRealtime(0.5f);
        blur3.SetActive(true);
        for (int i = 0; i < rodillo1.Length; i++)
        {
            randomNumber = UnityEngine.Random.Range(0,3);
            rodillo3[i].sprite = figuras[randomNumber];
            figurasRodillo3[i] = randomNumber;
        }

        //Dejamos la animacion de los rodillos durante 3 segundos
        yield return new WaitForSecondsRealtime(3f);
        //Desactivamos las animaciones de los rodillos en intervalos de 0.5 segundos
        blur1.SetActive(false);
        yield return new WaitForSecondsRealtime(0.5f);
        blur2.SetActive(false);
        yield return new WaitForSecondsRealtime(0.5f);
        blur3.SetActive(false);

        //Comprobamos si las 3 figuras centrales son iguales y si es asi activamos el sonido y mensaje de premio
        if(figurasRodillo1[1] == figurasRodillo2[1] && figurasRodillo1[1] == figurasRodillo3[1])
        {
            Debug.Log("premio");
            _audioSource.PlayOneShot(audioClips[1]);
            mensajePremio.SetActive(true);
        }
        //Volvemos a poner que se pueda interactuar con el boton
        playButton.interactable = true;
    }
}